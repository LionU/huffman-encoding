//////////////////////////////////////////////////////////////////////////////////////////////////
//		dest � sour �������� ��������� � ��������� ��������.
//		usize � csize - �������� ������ �������� ������ � ������ ������ �������������� ������ �� ��������. 
//		����� ������������ ���� ��������� �������� ������ � sour ����� � ���������� ��� ������ � usize. 
//		csize ���� ������ ������� ������� ��������, ������� �� ���������� � dest ���������. 
//		����� ������������ ���� ��������� � sour �������������� ������(������������ ����� ����) 
//		� ������ � get_uncompressed_size(sour), ������ dest ������ ������� �� ������ ��������. 
//		����� ���������� ���������� usize ����� ����������� � �������� �������� ������.
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "huffman.h"

//////////////////////////////////////////////////////////////////////////////////////////////////

 /*   ������� sour = �������� ��� usize BYTES
      �������� dest = ������ ��� csize BYTES		
	  sour = source = ��������
	  dest = destination = ����������	*/

//////////////////////////////////////////////////////////////////////////////////////////////////
void Huffman::encode(unsigned char *dest, int &csize, unsigned char *sour, int usize)
{
        pheader = (PHUFFHDR)dest;     //huff ���������
        psour = sour;                //sour �����
        pdest = dest;                //dest �����
        filesize = usize;            //������ ��������� �����
        csize = 0;                   //������ �� ������

        symnum = 0;
        bitslast = 8;


        storetree();                 //��������� ������� � ���������� ����������
        buildtree();                 //���������� ������ ��������
        buildbook();                 //���������� ������ ��������� (codebook)
        for (int i = 0; i < filesize; i++)
                writesymb(*psour++);


        csize = int((pdest - dest) + 1);
        pheader->cmpsize = csize;
}
//////////////////////////////////////////////////////////////////////////////////////////////////

/*    ������� sour = ������
      �������� dest = �������� ��� usize BYTES     */
//////////////////////////////////////////////////////////////////////////////////////////////////
void Huffman::decode(unsigned char *dest, int &usize, unsigned char *sour)
{
        pheader = (PHUFFHDR)sour;     //huff ���������
        psour = sour;                //sour �����
        pdest = dest;                //dest �����
        usize = 0;                   //������ �� ������

        bitslast = 8;


        readtree();                  //������ ��������� � �������� ������� (������������� ��������)
        buildtree();                 //���������� ������
        while (filesize) {
                *pdest++ = readsymb();
                filesize--;
        }


        usize = int(pdest - dest);
}
//////////////////////////////////////////////////////////////////////////////////////////////////



/*                           (!) �����������                             */

/*         ������������� ������� ������
           ���������� ����������                             */
void Huffman::storetree(void)
{
        /*               ������������� ������                           */
        memset(tmass1, 0, 256*sizeof(hufftree));
        memset(tmass2, 0, 256*sizeof(hufftree));
        memset(smass, 0, 256*sizeof(huffsymbol));

        temptree = (PHTREE)tmass1;
        htree = (PHTREE)tmass2;
        hsymbol = (PSYMB)smass;

        for (int i = 0; i < 256; i++) temptree[i].s1 = i;               //������������� ��������
        for (int i = 0; i < filesize; i++) temptree[psour[i]].freq++;   //��������� ������� ������������� ��������
        for (int i = 0; i < 256; i++) 
                if (temptree[i].freq) symnum++;        //��������� ���� ��������
        
        qsort((void *)temptree, 256, sizeof(hufftree), &compare);      //���������� ���������� (!) ������


        /*               ������������� ����������                           */
        memcpy(pheader->magic, "HUFF", 4);
        pheader->symnum = symnum;
        pheader->uncmpsize = filesize;
        pheader->cmpsize = 0;

        pdest += sizeof(huffheader);

        /*   �������� [  freq  ][C] ���    */
        for (int i = 0; i < symnum; i++) {
                psheader = (PSYMBHDR)pdest;
                psheader->symb = temptree[i].s1;
                psheader->freq = temptree[i].freq;

                pdest += 5;     //sizeof(PSYMBFRQ)
        }
        *pdest = 0;
}

/*         ����������  htree �� temptree                */
void Huffman::buildtree()
{
        /*        ����������� ������ symnum = 1   */
        if (symnum == 1) {
                hsymbol[psour[0]].size = 1;
                return;
        }
        /*        ���������� 1 & 2 ��������        */
        for (int i = symnum - 2; i >= 0; i--) {
                if (temptree[i].parent) {                    //���� ��� ��� ����������� ����
                        htree[i].pleft = temptree[i].parent;        //�� ������ ���� 0
                        htree[i].pleft->parent = &htree[i];
                } else
                        htree[i].s1 = temptree[i].s1;               //�� ��������� ������ 0

                if (temptree[i+1].parent) {
                        htree[i].pright = temptree[i+1].parent;     //�� ������ ���� 1
                        htree[i].pright->parent = &htree[i];
                } else
                        htree[i].s2 = temptree[i+1].s1;	           //�� ��������� ������ 1

                if (i) {
                        temptree[i].freq += temptree[i+1].freq;     //[i]+[i+1] ������� ��� ������ [i] ����
                        temptree[i].parent = &htree[i];             //[i] ���������� �����
                        temptree[i+1].freq = 0;                     //[i+1] ��������

                        qsort((void *)temptree, (i + 1), sizeof(hufftree), &compare);
                }
        }
}

/*         ���������� ������ ��������� (codebook) �� htree ������     */
void Huffman::buildbook()
{
        unsigned char c;
        PHTREE parent, child;

        /*     ���������� �������� �� htree    */
        for (int i = symnum - 2; i >= 0; i--) {
                if (!htree[i].pleft) {            //0bit ������
                        c = htree[i].s1;
                        hsymbol[c].size++;

                        child = &htree[i];
                        parent = child->parent;
                        while (parent) {
                                if (parent->pleft == child)              //��������� 0 � ���� ������� 
                                        hsymbol[c].size++;
                                if (parent->pright == child)		       //��������� 1 � ���� �������
                                        hsymbol[c].symb |= __int64(1 << hsymbol[c].size++);

                                child = parent;                    //child ���������� parent
                                parent = child->parent;            //parent ���������� 'childs' parent
                        }
                }
                if (!htree[i].pright) {           //1bit ������
                        c = htree[i].s2;
                        hsymbol[c].symb |= __int64(1 << hsymbol[c].size++);

                        child = &htree[i];
                        parent = child->parent;
                        while (parent) {
                                if (parent->pleft == child)              //��������� 0 � ���� �������
                                        hsymbol[c].size++;
                                if (parent->pright == child)             //��������� 1 � ���� �������
                                        hsymbol[c].symb |= __int64(1 << hsymbol[c].size++);

                                child = parent;                    //child ���������� parent
                                parent = child->parent;            //parent ���������� 'childs' parent
                        }
                }
        }
}

/*         ������ ������ ����� �������� � dest    */
void Huffman::writesymb(int c)
{
        for (unsigned int i = 1; i <= hsymbol[c].size; i++) {
                *pdest |= ((0x1 & (hsymbol[c].symb >> (hsymbol[c].size - i))) << (--bitslast));

                if (bitslast == 0) {
                        *(++pdest) = 0;
                        bitslast = 8;
                }
        }
}

/*                           �������������                              */

/*         ������ huff ���������� � ��������� ������        */
void Huffman::readtree()
{
        /*               ������������� ������                    */
        memset(tmass1, 0, 256*sizeof(hufftree));
        memset(tmass2, 0, 256*sizeof(hufftree));

        temptree = (PHTREE)tmass1;
        htree = (PHTREE)tmass2;

        filesize = pheader->uncmpsize;
        symnum = pheader->symnum;
        psour += sizeof(huffheader);

        /*       ��������� ������      */
        for (int i = 0; i < symnum; i++) {
                psheader = (PSYMBHDR)psour;
                temptree[i].freq = psheader->freq;
                temptree[i].s1 = psheader->symb;

                psour += 5;      //sizeof(PSYMBFRQ)
        }
}

/*         ������������� �������� huff �� ����� �� ���������        */
unsigned char Huffman::readsymb()
{
        PHTREE node = &htree[0];

        while (1) {
                if (getnextbit()) {           //��������� ������ ���� right node
                        if (node->pright)
                                node = node->pright;
                        else
                                return node->s2;
                } else {              //��������� ����� ���� left node
                        if (node->pleft)
                                node = node->pleft;
                        else
                                return node->s1;
                }
        }
}

/*         ��������� qsort()              */
int __cdecl Huffman::compare(const void *a, const void *b)
{
        PHTREE t1 = (PHTREE)a;
        PHTREE t2 = (PHTREE)b;

        if (t1->freq == t2->freq) return 0;
        else return ((t1->freq < t2->freq) ? 1 : -1);
}
