
#pragma once
#pragma warning(disable : 4996)

#include <stdio.h>
#include <tchar.h>
#include <memory.h>
#include <stdlib.h>

#ifndef Huffman_h
#define Huffman_h


/*       huffman �������            */
typedef struct hufftree {
        unsigned int freq;       //s1 freq
        unsigned char s1;        //0...255:codes       left symb
        unsigned char s2;        //                    right symb
        hufftree* pleft;         //0
        hufftree* pright;        //1
        hufftree* parent;        //parent node
} *PHTREE;

typedef struct huffsymbol {
        unsigned int size;       //������ ������� � �����
        __int64 symb;            //max 256 bits
} *PSYMB;

/*       huffman file headers            */
typedef struct huffheader {
        char magic[4];           //'HUFF'
        int symnum;              //�������� ����� ���� ��������
        int uncmpsize;           //������ ��������� �����
        int cmpsize;             //������ ������� �����
} *PHUFFHDR;

typedef struct symbheader {
        unsigned int freq;       //��� ������� ������������� �������
        unsigned char symb;      //������
} *PSYMBHDR;


class Huffman
{
public:
        Huffman(){};
        //Huffman(const Huffman& huffman);
        //~Huffman();

// ��������
        void encode(unsigned char *dest, int &csize, unsigned char *sour, int usize);
        void decode(unsigned char *dest, int &usize, unsigned char *sour);

// ������
// ������
        inline int get_uncompressed_size(unsigned char *sour);

protected:
private:
        Huffman(const Huffman& huffman);
        const Huffman& operator=(const Huffman& huffman);
        
        
        PHTREE temptree;                              //tmass1 ������ ������
        PHTREE htree;                                 //tmass2 ������ huff ������
        PSYMB hsymbol;                                //smass ���������� ��������� -������ (codebook)

        unsigned char tmass1[256*sizeof(hufftree)];             //������ ������ � �������� �����
        unsigned char tmass2[256*sizeof(hufftree)];             //��������� ����������� ������
        unsigned char smass[256*sizeof(huffsymbol)];            //���������� ��������� (codebook). ����� [code][size] ���

        unsigned char *psour;                                  //��������� ���������
        unsigned char *pdest;                                  //��������� ����������

        PHUFFHDR pheader;                              //��������� huffman �����
        PSYMBHDR psheader;                             //[ freq ][C] ����

        int filesize;                                 //������ ��������� �����
        int symnum;                                   //��� ���������(!) �������
        int bitslast;
        
        
        static int __cdecl compare(const void *a, const void *b);    //��� ����������

        void storetree(void);                         //������������� ������ � huff headers (����������)
        void buildtree(void);                         //���������� htree
        void buildbook(void);                         //���������� ���������� ��������� (codebook) [code][size] ���
        void writesymb(int c);                        //write codebook symbol

        void readtree(void);                          //������ ���������� � ��������� ������
        unsigned char readsymb();                     //������ �������� �� ������ htree
        inline int getnextbit();                      //��������� ����� �� sour
                     
};

inline int Huffman::get_uncompressed_size(unsigned char *sour)
{
        pheader = (PHUFFHDR)sour;
        return pheader->uncmpsize;
}

/*             ��������� ��������� ����� �� sour            */
inline int Huffman::getnextbit()
{
        if (bitslast) {
                return 0x1 & (*psour >> (--bitslast));
        } else {
                psour++;
                bitslast = 8;
                return 0x1 & (*psour >> (--bitslast));
        }
}

#endif